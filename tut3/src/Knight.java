import javafx.animation.AnimationTimer;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;

public class Knight extends ImageView {
	double imgWidth = 500;
	double imgHeight = 400;
	Image[] knight = new Image[7];
	GraphicsContext gc;
	
	public Knight(GraphicsContext gc) {
		for (int i=0; i<7; i++) {
			knight[i] = new Image("_PNG/1_KNIGHT/_ATTACK/ATTACK_00" + i + ".png", imgWidth, imgHeight, false, false);
		}
		this.gc = gc;
	}
	
	public AnimationTimer attack = new AnimationTimer() {
		int i=0;
		long lastUpdate = 0;
		long refreshDelay = 300*1000*1000;
		
		@Override
		public void handle(long now) {
			if (i<7)
				i++;
			if (i==7)
				i=0;
			// clear the canvas
			if (now - lastUpdate >= refreshDelay) {
				gc.setFill(Color.BLACK);
				gc.fillRect(0, 0, imgWidth, imgHeight);
				gc.drawImage(knight[i], 0, 0);
				lastUpdate = now;
			}
		}
	};
}
