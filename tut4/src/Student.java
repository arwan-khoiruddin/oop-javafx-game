import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.util.Random;

public class Student extends ImageView {
	private boolean isGirl;
	
	public boolean isGirl() {
		return isGirl;
	}
	
	public void setGirl(boolean girl) {
		isGirl = girl;
		if (isGirl) {
			this.setImage(new Image("girl-student.jpg"));
		} else {
			this.setImage(new Image("boy-student.jpg"));
		}
	}
	
	public Student() {
		this.setFitWidth(200);
		this.setPreserveRatio(true);
		
		this.setX(StudentGroup.XPos);
		this.setY(100);
		StudentGroup.XPos += this.getFitWidth();
	}
}
