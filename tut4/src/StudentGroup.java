import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

public class StudentGroup extends Application {
	
	public static double XPos = 0;
	
	public static void main(String[] args) {
		launch(args);
	}
	
	private void writeText(GraphicsContext gc, String text) {
		int x = 0;
		int y = 50;
		
		gc.setFill( Color.RED );
		gc.setStroke( Color.BLACK );
		gc.setLineWidth(2);
		Font theFont = Font.font( "Times New Roman", FontWeight.BOLD, 48 );
		gc.setFont( theFont );
		gc.fillText( text, x, y);
		gc.strokeText( text, x, y);
		
	}
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		primaryStage.setTitle("Group of students");
		Group root = new Group();
		Scene scene = new Scene(root);
		primaryStage.setScene(scene);
		
		Canvas canvas = new Canvas(1000, 500);
		GraphicsContext gc = canvas.getGraphicsContext2D();
		
		
		root.getChildren().add(canvas);
		
		Student[] students = new Student[5];

		students[0] = new Student();
		students[0].setGirl(true);

		students[1] = new Student();
		students[1].setGirl(false);

		students[2] = new Student();
		students[2].setGirl(true);

		students[3] = new Student();
		students[3].setGirl(false);

		students[4] = new Student();
		students[4].setGirl(true);

		root.getChildren().addAll(students);
		
		// DO YOUR WORK BELOW
		
		// Count how many girls and how many boys?
		
		// write the result in your canvas using the following way
		
		// writeText(gc, numBoys + " boys and " + numGirls + " girls");
		
		primaryStage.show();
	}
}
